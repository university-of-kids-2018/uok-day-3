package com.ocado.uok.robot.result;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.MutableLiveData;
import android.content.Context;
import android.util.Log;

import com.ocado.uok.robot.time.TimeRecord;

import org.apache.commons.io.IOUtils;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Model dla listy wyników zadań.
 * Potrafi zapisywać wynik, pobierać listę, a także zamieniać wynik na wiersz tabeli.
 */
public class ShoppingTaskResults extends AndroidViewModel {
    private static final String RESULT_FILE_NAME = "results.json";
    private static final String ENCODING = "UTF-8";

    private final Context appContext;
    private final Serializer serializer = new Serializer();

    private MutableLiveData<List<TimeRecord>> records = new MutableLiveData<>();

    public ShoppingTaskResults(Application application) {
        super(application);
        this.appContext = application.getApplicationContext();
        this.records.setValue(this.retrieveAll());
    }

    /**
     * Daje listę wyników (opakowane w MutableLiveData, aby można było śledzić zmiany).
     */
    public MutableLiveData<List<TimeRecord>> getTimeRecords() {
        return records;
    }

    /**
     * Zapisuje wynik zadania
     */
    public void save(TimeRecord timeRecord) {
        FileOutputStream stream = null;
        try {
            List<TimeRecord> timeRecords = records.getValue();
            timeRecords.add(timeRecord);
            stream = appContext.openFileOutput(RESULT_FILE_NAME, Context.MODE_PRIVATE);
            IOUtils.write(serializer.serialize(timeRecords), stream, ENCODING);
            records.postValue(timeRecords);
        } catch (IOException e) {
            Log.w("ShoppingTaskResults", "Failed to store shopping tasks", e);
        } finally {
            IOUtils.closeQuietly(stream);
        }
    }

    private List<TimeRecord> retrieveAll() {
        FileInputStream stream = null;
        try {
            stream = appContext.getApplicationContext().openFileInput(RESULT_FILE_NAME);
            return serializer.deserialize(IOUtils.toString(stream, ENCODING));
        } catch (IOException e) {
            Log.w("ShoppingTaskResults", "Failed to read shopping tasks", e);
        } finally {
            if(stream != null) {
                IOUtils.closeQuietly(stream);
            }
        }
        return new ArrayList<>();
    }

}
